package com.whitewalkers.service;

import com.whitewalkers.controller.response.ConfusedItem;
import com.whitewalkers.controller.response.Restaurant;
import com.whitewalkers.dao.RestaurantMenuDao;
import com.whitewalkers.dao.SwipeHistoryDao;
import com.whitewalkers.entity.CategoryWiseWeight;
import com.whitewalkers.entity.RestaurantMenuEntity;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by ajaykumar.rai on 7/15/17.
 */
@Service
public class ListingService {

    @Autowired
    SwipeHistoryDao swipeHistoryDao;

    @Autowired
    RestaurantMenuDao menuDao;

    @Autowired
    SolrService solrService;

    static Map<String, List<ConfusedItem>> uidItemMamp = new HashMap<>();

    public List<ConfusedItem> getList(String uid, String latlng) {
        List<ConfusedItem> list = new ArrayList<>();
        if (uidItemMamp.containsKey(uid)) {
            list = uidItemMamp.get(uid);
        } else {

            List<CategoryWiseWeight> fromUser = getCategoryWeightList(swipeHistoryDao.findByCustomerId(Integer.valueOf(uid)));
            List<CategoryWiseWeight> fromOthers = getCategoryWeightList(swipeHistoryDao.findByNotCustomerId(Integer.valueOf(uid)));

            Map<String, CategoryWeight> categoryWeightMap = new HashMap<>();
            List<String> categories = new LinkedList<>();

            fromUser.forEach(categoryWiseWeight -> {
                if(!categoryWeightMap.containsKey(categoryWiseWeight.getCategory())){
                    CategoryWeight categoryWeight = new CategoryWeight();
                    categoryWeight.setName(categoryWiseWeight.getCategory());
                    categoryWeight.setFromUser(categoryWiseWeight.getTotalWeight());
                    categoryWeightMap.put(categoryWiseWeight.getCategory(),categoryWeight);
                }else{
                    CategoryWeight categoryWeight = categoryWeightMap.get(categoryWiseWeight.getCategory());
                    categoryWeight.setFromUser(categoryWiseWeight.getTotalWeight());
                    categoryWeightMap.put(categoryWiseWeight.getCategory(),categoryWeight);
                }
            });

            fromOthers.forEach(categoryWiseWeight -> {
                if(!categoryWeightMap.containsKey(categoryWiseWeight.getCategory())){
                    CategoryWeight categoryWeight = new CategoryWeight();
                    categoryWeight.setFromOtherUsers(categoryWiseWeight.getTotalWeight());
                    categoryWeightMap.put(categoryWiseWeight.getCategory(),categoryWeight);
                }else{
                    CategoryWeight categoryWeight = categoryWeightMap.get(categoryWiseWeight.getCategory());
                    categoryWeight.setName(categoryWiseWeight.getCategory());
                    categoryWeight.setFromOtherUsers(categoryWiseWeight.getTotalWeight());
                    categoryWeightMap.put(categoryWiseWeight.getCategory(),categoryWeight);
                }
            });

            List<CategoryWeight> categoryWeightList;
            categoryWeightList = new ArrayList<>(categoryWeightMap.values());
            Collections.sort(categoryWeightList, new CategoryWeightComparator());


            List<Restaurant> restaurants = solrService.getRestaurants(latlng,4,1, 200);
            List<Integer> rIds = restaurants.stream().map(restaurant -> restaurant.getId()).collect(Collectors.toList());
            List<RestaurantMenuEntity> menuEntities = menuDao.findByComboAndRestIdIn(1,rIds);

            Map<String,List<RestaurantMenuEntity>> categoryWiseList = menuEntities.stream().collect(Collectors.groupingBy(RestaurantMenuEntity::getCategory));

            return getItemsToShow(categoryWiseList,categoryWeightList);


            //list = convertEntitiesToList(items);
        }

        return list;
    }

    private List<ConfusedItem> getItemsToShow(Map<String, List<RestaurantMenuEntity>> categoryWiseMap, List<CategoryWeight> categoryWeightList) {
        int count = 10;
        int max=3;
        List<ConfusedItem> list = new ArrayList<>();
        Iterator iterator = categoryWeightList.iterator();

        while (count>0 && iterator.hasNext()){
            String category = ((CategoryWeight) iterator.next()).getName();
            if (!categoryWiseMap.containsKey(category) && categoryWiseMap.get(category)==null) continue;
            List<RestaurantMenuEntity> temp = categoryWiseMap.get(category);
            temp = temp.subList(0,max>=temp.size()?temp.size():max);
            if (temp.size()<=0) continue;
            list.addAll(getConfusedList(temp,category));
            max = max>1?max-1:1;
        }

        return list;
    }

    private List<ConfusedItem> getConfusedList(List<RestaurantMenuEntity> temp,String category) {
        List<ConfusedItem> list = new LinkedList<>();
        temp.forEach( menuEntity -> {
            list.add(getConfusedItem(menuEntity));
        });

        return list;
    }

    private void sortCategories(List<String> categories, Map<String, CategoryWeight> categoryWeightMap) {
        for (Map.Entry<String, CategoryWeight> entry : categoryWeightMap.entrySet()){

        }
    }

    private List<CategoryWiseWeight> getCategoryWeightList(List<Object[]> objectList) {
        List<CategoryWiseWeight> list = new LinkedList<>();
        objectList.forEach(obj -> {
            CategoryWiseWeight categoryWiseWeight = new CategoryWiseWeight();
            categoryWiseWeight.setCategory(obj[0].toString());
            categoryWiseWeight.setTotalWeight(Integer.valueOf(obj[1].toString()));
            list.add(categoryWiseWeight);
        });
        return list;
    }


    private ConfusedItem getConfusedItem(RestaurantMenuEntity menuEntity) {
        ConfusedItem confusedItem = new ConfusedItem();
        confusedItem.setItemId(menuEntity.getId());
        confusedItem.setItemName(menuEntity.getItemName());
       // confusedItem.setRestName(menuEntity.getRestId());
        confusedItem.setCategory(menuEntity.getCategory());
       // confusedItem.setCloudinaryId(menuEntity.getC);
        confusedItem.setPrice(menuEntity.getItemPrice());
        confusedItem.setSla(new Random().nextInt(50) + " minutes");
        return confusedItem;
    }

    @NoArgsConstructor
    @Data
    static class CategoryWeight{
        private String name;
        private double fromUser;
        private double fromOtherUsers;
        public double getEffectiveWeight(){
            return this.fromOtherUsers + 2*this.fromUser;
        }

    }

    static class CategoryWeightComparator implements Comparator<CategoryWeight>{


        @Override
        public int compare(CategoryWeight o1, CategoryWeight o2) {
            if (o1.getEffectiveWeight()<o2.getEffectiveWeight()){
                return 1;
            }

            if (o1.getEffectiveWeight()>o2.getEffectiveWeight()){
                return -1;
            }
            return 0;
        }
    }



}
