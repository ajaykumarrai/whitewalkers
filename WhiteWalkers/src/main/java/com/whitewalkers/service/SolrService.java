package com.whitewalkers.service;

import com.whitewalkers.controller.response.Restaurant;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.params.CommonParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by anuj.jalan on 16/07/17.
 */
@Component
public class SolrService {

    @Autowired
    private SolrClient solrClient;

    public List<Restaurant> getRestaurants(String latLng, int distanceRadius, int pageNo, int pageSize) {
        SolrQuery query = new SolrQuery();
        query.set("fq", "{!geofilt}");
        query.set("sfield", "lat_lng");
        query.set("pt", latLng);
        query.set("d", distanceRadius);
        query.setStart((pageNo - 1) * pageSize);
        query.setRows(pageSize);
        query.setParam(CommonParams.FL, "*");
        query.setParam(CommonParams.Q, "*:*");
        List<Restaurant> restaurantList = new ArrayList<>();
        try {
            QueryResponse queryResponse = solrClient.query(query);
            if (queryResponse != null && queryResponse.getResults() != null && queryResponse.getResults().getNumFound() > 0) {
                SolrDocumentList solrDocumentList = queryResponse.getResults();
                for (SolrDocument solrDocument : solrDocumentList) {
                    Restaurant restaurant = Restaurant.builder()
                            .area_id((Integer) solrDocument.getFieldValue("area_id"))
                            .avg_rating((Double) solrDocument.getFieldValue("avg_rating"))
                            .city_id((Integer) solrDocument.getFieldValue("city_id"))
                            .cuisine((String) solrDocument.getFieldValue("cuisine"))
                            .id((Integer) solrDocument.getFieldValue("id"))
                            .lat_long((String) solrDocument.getFieldValue("lat_long"))
                            .name((String) solrDocument.getFieldValue("name"))
                            .build();
                    restaurantList.add(restaurant);
                }
                return restaurantList;
            }
        } catch (SolrServerException e) {
            e.printStackTrace();
            return restaurantList;
        } catch (IOException e) {
            e.printStackTrace();
            return restaurantList;
        }
        return restaurantList;
    }
}
