package com.whitewalkers;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by anuj.jalan on 16/07/17.
 */
@Configuration
public class AppConfig {

    @Bean
    public SolrClient solrClient() {
        String urlString = "http://172.16.120.177:8983/solr/restaurants";
        SolrClient solr = new HttpSolrClient.Builder(urlString).build();
        return solr;
    }
}


