package com.whitewalkers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by anuj.jalan on 15/07/17.
 */
@SpringBootApplication(scanBasePackages = "com.whitewalkers")
@EnableJpaRepositories(basePackages = "com.whitewalkers")
@ComponentScan(basePackages = "com.whitewalkers")
@EnableTransactionManagement
@EnableScheduling
@EnableWebMvc
@EnableAutoConfiguration
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
