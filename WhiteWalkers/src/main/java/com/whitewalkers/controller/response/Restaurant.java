package com.whitewalkers.controller.response;

import lombok.*;

/**
 * Created by anuj.jalan on 16/07/17.
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Restaurant {

    public String lat_long;
    public double avg_rating;
    public String name;
    public String cuisine;
    public int id;
    public int area_id;
    public int city_id;

}
