package com.whitewalkers.controller.response;

import lombok.Data;

import java.util.List;

/**
 * Created by ajaykumar.rai on 7/15/17.
 */
@Data
public class ConfusedList extends BaseResponse {

    List<ConfusedItem> responses;


}
