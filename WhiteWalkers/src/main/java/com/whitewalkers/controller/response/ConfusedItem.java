package com.whitewalkers.controller.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * Created by ajaykumar.rai on 7/15/17.
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ConfusedItem {
    private long itemId;
    private String itemName;
    private String restName;
    private String cloudinaryId;
    private float price;
    private String description;
    private String sla;
    private String category;
    private float calories;
}
