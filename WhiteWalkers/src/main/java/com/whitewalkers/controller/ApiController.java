package com.whitewalkers.controller;

import com.whitewalkers.controller.response.ConfusedItem;
import com.whitewalkers.controller.response.ConfusedList;
import com.whitewalkers.service.ListingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by ajaykumar.rai on 7/15/17.
 */
@Controller
@RequestMapping("/api")
public class ApiController {

    @Autowired
    ListingService listingService;

    @RequestMapping("/hello")
    public String getHello() {
        return "Hello successful";
    }

    @RequestMapping("/confused/list/{uid}")
    @ResponseBody
    public ConfusedList getList(@PathVariable String uid, @RequestParam(name = "lat", required = true)String lat,@RequestParam(name="lng", required = true)String lng) {
        double latitude = Double.parseDouble(lat);
        double longitude =  Double.parseDouble(lng);
        List<ConfusedItem> list = listingService.getList(uid,latitude+","+longitude);
        ConfusedList response = new ConfusedList();
        response.setResponses(list);
        return response;
    }
}
