package com.whitewalkers.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by ajaykumar.rai on 7/15/17.
 */
@Data
@Entity
@Table(name = "restaurant_menu")
public class RestaurantMenuEntity {

    @Id
    @GeneratedValue
    @Column(name = "id",updatable = false)
    private long id;

    @Column(name = "item_name")
    private String itemName;

    @Column(name = "restaurant_id")
    private int restId;

    @Column(name = "is_enabled")
    private int enabled;

    @Column(name = "item_price")
    private int itemPrice;

    @Column(name = "category")
    private String category;

    @Column(name = "combo")
    private int combo;


}
