package com.whitewalkers.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by ajaykumar.rai on 7/16/17.
 */
@Data
@Entity
@Table(name = "order_details")
public class OrderDetailsEntity {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column( name = "customer_id")
    private int custId;

    @Column(name="restaurant_d")
    private int restId;

}
