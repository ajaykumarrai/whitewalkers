package com.whitewalkers.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ajaykumar.rai on 7/16/17.
 */
@Data
@NoArgsConstructor
public class CategoryWiseWeight {

    private String category;
    private double totalWeight;

}
