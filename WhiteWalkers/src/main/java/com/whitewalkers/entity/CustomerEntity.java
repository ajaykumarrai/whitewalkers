package com.whitewalkers.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by ajaykumar.rai on 7/16/17.
 */
@Data
@Entity
@Table(name = "customer")
public class CustomerEntity {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;
}
