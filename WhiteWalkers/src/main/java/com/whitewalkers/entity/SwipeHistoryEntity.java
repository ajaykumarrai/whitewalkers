package com.whitewalkers.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by ajaykumar.rai on 7/16/17.
 */
@Data
@Entity
@Table(name = "swipe_history")
public class SwipeHistoryEntity {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column( name = "customer_id")
    private int customerId;

    @Column( name = "item_id")
    private int itemId;

    @Column( name = "gaze_time")
    private double gazeTime;

    @Column( name = "weight")
    private double weight;
}
