package com.whitewalkers.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by ajaykumar.rai on 7/16/17.
 */
@Data
@Entity
@Table(name = "order_items")
public class OrderItems {

    @Id
    @GeneratedValue
    @Column( name = "id")
    private int id;

    @Column( name = "order_id")
    private long orderId;

    @Column(name = "item_id")
    private long itemId;

    @Column( name = "quantity")
    private int quantity;
}
