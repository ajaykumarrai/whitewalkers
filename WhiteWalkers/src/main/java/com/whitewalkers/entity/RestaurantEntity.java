package com.whitewalkers.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by ajaykumar.rai on 7/15/17.
 */
@Data
@Entity
@Table(name = "restaurant")
public class RestaurantEntity {

    @Id
    @GeneratedValue
    @Column(name = "id",updatable = false)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "cuisine")
    private String cuisine;

    @Column(name = "area_id")
    private int areaId;

    @Column( name = "city_id")
    private int cityId;

    @Column(name = "lat")
    private  String lat;

    @Column(name = "lng")
    private  String lng;

    @Column(name = "avg_rating")
    private Double avgRating;



}
