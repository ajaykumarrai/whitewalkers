package com.whitewalkers.dao;

import com.whitewalkers.entity.RestaurantMenuEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by ajaykumar.rai on 7/15/17.
 */
@Repository("restaurantMenuDao")
public interface RestaurantMenuDao extends CrudRepository<RestaurantMenuEntity, Long> {

    List<RestaurantMenuEntity> findByComboAndRestIdIn(int combo,List<Integer> restIds);

}
