package com.whitewalkers.dao;

import com.whitewalkers.entity.SwipeHistoryEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by ajaykumar.rai on 7/16/17.
 */
@Resource
public interface SwipeHistoryDao extends CrudRepository<SwipeHistoryEntity, Integer> {

    @Query(value = "select category, sum(weight) as total_weight from swipe_history sh join restaurant_menu rm where sh.customer_id=? and rm.combo=1 group by rm.category order by total_weight desc limit 20 ", nativeQuery = true)
    List<Object[]> findByCustomerId(int customerId);

    @Query(value = "select category, sum(weight) as total_weight from swipe_history sh join restaurant_menu rm where sh.customer_id!=? and rm.combo=1 group by rm.category order by total_weight desc limit 20 ", nativeQuery = true)
    List<Object[]> findByNotCustomerId(int customerId);

}
